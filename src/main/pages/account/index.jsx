import React from 'react';
import { IonHeader, IonContent, IonAvatar, IonButton } from '@ionic/react';

import './Account.scss';

import Context from '../../app/Context/index';

class Account extends React.Component {
    static contextType = Context;

    render() {
        const { user } = this.context;
        return (
            <>
                <IonContent className="account" scroll-y="false">
                    <div className="account__header">
                        <div className="account-background">
                        <div className="account-background-mask"></div>
                        </div>
                        <div className="account-card">
                            <IonAvatar className="account__header__image">
                                <img src={user.imageUrl} alt="avatar" />
                            </IonAvatar>

                            <div className="account__header__name">
                                {user.name}
                            </div>

                            <button className="account__header__button" >
                                Edit Profile
                            </button>
                        </div>
                    </div>

                    {/*
                    <IonButton fill="clear" color="success">
                        Edit Profile
                    </IonButton>

 */}
                </IonContent>
            </>
        );
    }
}

export default Account;
