import React, { useContext } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { IonSpinner } from '@ionic/react';
import { AuthContext } from '../auth';
import { ROUTE_LOGIN } from '../../utils/routes';

const PrivateRoute = (props) => {
    const { component: RouteComponent, ...rest } = props;
    const { currentUser } = useContext(AuthContext);

    return (
        <Route
            {...rest}
            render={routeProps => {
                if (currentUser === undefined) {
                    return (
                        <div className="spinner-container">
                            <IonSpinner name="dots" />
                        </div>
                    );
                }

                return currentUser !== null ? (
                    <RouteComponent {...routeProps} />
                ) : (
                    <Redirect to={ROUTE_LOGIN} />
                );
            }}
        />
    );
};

export default PrivateRoute;
