import React from 'react';
import { IonButton, getPlatforms, IonLabel, IonSpinner } from '@ionic/react';

import './Restaurant.scss';
import Event from '../event';
import api from '../../api';
import EventAdd from '../eventAdd';
import { AuthContext } from '../auth';

class Restaurant extends React.Component {
    static contextType = AuthContext;

    constructor(props) {
        super(props);
        this.state = {
            restaurant: this.props.restaurant,
            myEvents: [],
            otherEvents: [],
            fetchingRestaurant: false
        };
    }

    onClosePress = () => {
        this.setState({
            myEvents: [],
            otherEvents: []
        });
        this.props.onClosePress();
    };

    onEventChange = () => {
        const { currentUser } = this.context;
        this.fetchEventsForRestaurant(this.state.restaurant.api.id).then(
            events =>
                this.setState({
                    myEvents: events.filter(
                        event => event.owner === currentUser.uid
                    ),
                    otherEvents: events.filter(
                        event => event.owner !== currentUser.uid
                    )
                })
        );
    };

    fetchRestaurant = id => {
        const { currentUser } = this.context;
        this.setState({
            fetchingRestaurant: true
        });
        api.fetchRestaurant(id)
            .then(restaurantQuery => {
                if (restaurantQuery.size > 0) {
                    const restaurant = restaurantQuery.docs[0];
                    this.setState({
                        restaurant: {
                            ...this.state.restaurant,
                            api: {
                                ...restaurant.data(),
                                id: restaurant.id
                            }
                        }
                    });
                    return this.fetchEventsForRestaurant(restaurant.id);
                }
            })
            .then(events => {
                if (events) {
                    this.setState({
                        myEvents: events.filter(
                            event => event.owner === currentUser.uid
                        ),
                        otherEvents: events.filter(
                            event => event.owner !== currentUser.uid
                        )
                    });
                }
            })
            .finally(() => {
                this.setState({
                    fetchingRestaurant: false
                });
            });
    };

    fetchEventsForRestaurant = async id => {
        const events = [];
        let eventsQuery = await api.fetchEventsForRestaurant(id);
        if (eventsQuery.size > 0) {
            for (let i = 0; i < eventsQuery.docs.length; i++) {
                const eventQuery = eventsQuery.docs[i];
                let participantsQuery = await api.fetchParticipantsCountForEvent(
                    eventQuery.id
                );
                events.push({
                    ...eventQuery.data(),
                    id: eventQuery.id,
                    participants: participantsQuery.docs.map(particiantQuery =>
                        particiantQuery.data()
                    )
                });
            }
        }
        return events;
    };

    renderEvents = (title, events, areMine) => {
        if (!events.length) {
            return null;
        }

        const { restaurant } = this.state;

        return (
            <>
                <IonLabel className="events__title">
                    {title}
                    {restaurant && restaurant.api && areMine && (
                        <EventAdd
                            restaurant={this.state.restaurant.api.id}
                            onEventChange={this.onEventChange}
                        />
                    )}
                </IonLabel>
                <div className="restaurant__events">
                    {events.map((event, index) => (
                        <Event
                            key={index}
                            event={event}
                            allowParticipate={!areMine}
                            onEventChange={this.onEventChange}
                        />
                    ))}
                </div>
            </>
        );
    };

    componentDidUpdate(prevProps) {
        if (prevProps.restaurant !== this.props.restaurant) {
            this.setState({
                restaurant: this.props.restaurant
            });
        }

        if (
            prevProps.restaurant !== this.props.restaurant &&
            this.props.restaurant !== null &&
            this.props.restaurant.api !== null
        ) {
            this.fetchRestaurant(this.props.restaurant.id);
        }
    }

    render() {
        const {
            restaurant,
            myEvents,
            otherEvents,
            fetchingRestaurant
        } = this.state;

        return (
            <div
                className={`restaurant__wrapper 
                ${restaurant && 'restaurant__wrapper--opened'}`}
                data-platforms={getPlatforms()}
            >
                <div
                    className="restaurant__container"
                    data-platforms={getPlatforms()}
                >
                    {fetchingRestaurant && (
                        <div className="spinner-container">
                            <IonSpinner name="dots" />
                        </div>
                    )}
                    {!fetchingRestaurant && (
                        <>
                            <div
                                className="restaurant__image"
                                style={{
                                    backgroundImage: `url(${
                                        restaurant && restaurant.api
                                            ? restaurant.api.photo_url
                                            : 'https://inland-investments.com/sites/default/files/property/No-Photo-Available.jpg'
                                    })`
                                }}
                            />
                            <p className="restaurant__title">
                                {restaurant && restaurant.title}
                            </p>
                            <p className="restaurant__description">
                                {restaurant &&
                                    restaurant.api &&
                                    restaurant.api.description}
                            </p>
                            <hr />
                            {restaurant && !restaurant.api && (
                                <IonLabel className="restaurant__not-present">
                                    Restaurant not present in database. Please
                                    register it.
                                </IonLabel>
                            )}
                            {this.renderEvents('My Events', myEvents, true)}
                            {this.renderEvents('Other Events', otherEvents)}
                         
                        </>
                    )}
                </div>
                <div className="restaurant__close-wrapper">
                    <IonButton
                        className="restaurant__close"
                        data-platforms={getPlatforms()}
                        onClick={this.onClosePress}
                    >
                        Close
                    </IonButton>
                </div>
            </div>
        );
    }
}

export default Restaurant;
