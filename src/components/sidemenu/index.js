import React from 'react';
import {
    IonAvatar,
    IonButton,
    IonContent,
    IonIcon,
    IonItem,
    IonLabel,
    IonList,
    IonMenu,
    IonMenuToggle
} from '@ionic/react';
import { map, person, settings } from 'ionicons/icons';
import { withRouter } from 'react-router';

import { ROUTE_ACCOUNT, ROUTE_MAP, ROUTE_SETTINGS } from '../../utils/routes';

import './Sidemenu.scss';
import api from '../../api';

import { AuthContext } from '../auth';

const pages = [
    { title: 'Account', path: ROUTE_ACCOUNT, icon: person },
    { title: 'Map', path: ROUTE_MAP, icon: map },
    { title: 'Settings', path: ROUTE_SETTINGS, icon: settings }
];

class Sidemenu extends React.Component {
    static contextType = AuthContext;

    renderMenuItems = () => {
        const { history, location } = this.props;
        return pages.map(page => (
            <IonMenuToggle key={page.title}>
                <div
                    className={
                        page.path === location.pathname
                            ? 'sidemenu__item--activated'
                            : ''
                    }
                >
                    <IonItem button onClick={() => history.push(page.path)}>
                        <IonIcon slot="start" icon={page.icon} />
                        <IonLabel>{page.title}</IonLabel>
                    </IonItem>
                </div>
            </IonMenuToggle>
        ));
    };

    render() {
        const { id } = this.props;
        const { currentUser } = this.context;
        return (
            <IonMenu
                swipeGesture={false}
                side="start"
                content-id={id}
                className="sidemenu"
            >
                <IonContent  scroll-y="false">
                    <div className="sidemenu__header">
                        <IonAvatar>
                            <img
                                src="https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y"
                                alt="avatar"
                            />
                        </IonAvatar>
                        <IonLabel className="sidemenu__name">
                            {currentUser.displayName}
                        </IonLabel>
                    </div>
                    <hr className="sidemenu__separator" />
                    <IonList>{this.renderMenuItems()}</IonList>
                    <IonButton
                        className="sidemenu__signout"
                        onClick={api.logout}
                    >
                        Sign out
                    </IonButton>
                </IonContent>
            </IonMenu>
        );
    }
}

export default withRouter(Sidemenu);
