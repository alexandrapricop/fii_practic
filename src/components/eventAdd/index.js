import React from 'react';
import {
    IonButton,
    IonContent,
    IonInput,
    IonItem,
    IonLabel,
    IonList,
    IonModal,
    IonDatetime
} from '@ionic/react';

import './EventAdd.scss';
import api from '../../api';
import { handleEnterKeypress } from '../../utils/handlers';
import { AuthContext } from '../auth';

class EventAdd extends React.Component {
    static contextType = AuthContext;

    constructor() {
        super();
        this.formEl = React.createRef();
        this.state = {
            showModal: false
        };
    }

    showModal = show => {
        this.setState({
            showModal: show
        });
    };

    createEvent = async event => {
        event.preventDefault();
        const { currentUser } = this.context;
        const { name, participants, date } = event.target.elements;
        const eventQuery = await api.createEventForRestaurant({
            name: name.value,
            max_participants: +participants.value,
            date: new Date(date.value).getTime(),
            restaurant: this.props.restaurant,
            owner: currentUser.uid
        });

        const { id } = eventQuery;

        api.joinEvent(id, currentUser.uid).then(() => {
            this.props.onEventChange();
            this.showModal(false);
        });
    };

    componentDidMount() {
        document.addEventListener('keydown', this.handleEnterKeypresWrapper);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleEnterKeypresWrapper);
    }

    handleEnterKeypresWrapper = ev => {
        handleEnterKeypress(ev, () => {
            ev.preventDefault();
            this.formEl.current.dispatchEvent(new Event('submit'));
        });
    };

    render() {
        const { showModal } = this.state;
        return (
            <>
                <IonModal isOpen={showModal}>
                    <IonContent style={{
                        '--padding-start': '15px',
                        '--padding-end': '15px',
                        '--padding-top': '15px'
                    }}>
                        <IonLabel style={{ fontSize: 20, marginBottom: 20 }}>
                            Create new event
                        </IonLabel>
                        <IonList>
                            <form
                                ref={this.formEl}
                                className="form-container"
                                onSubmit={this.createEvent}
                            >
                                <IonItem>
                                    <IonLabel position="floating">
                                        Name
                                    </IonLabel>
                                    <IonInput name="name" />
                                </IonItem>
                                <IonItem>
                                    <IonLabel position="floating">
                                        Participants
                                    </IonLabel>
                                    <IonInput
                                        type="number"
                                        name="participants"
                                    />
                                </IonItem>
                                <IonItem>
                                    <IonLabel position="floating">
                                        Date
                                    </IonLabel>
                                    <IonDatetime
                                        name="date"
                                        displayFormat="MMM DD, YYYY HH:mm"
                                    />
                                </IonItem>
                                
                            </form>
                        </IonList>
                        <IonButton
                                    style={{position: 'absolute', bottom: 0, left: 0, margin: 0, width: '50%', borderRight: '1px solid white'}}
                                    type="button"
                                    onClick={() => this.formEl.current.dispatchEvent(new Event('submit'))}
                                >
                                    Create event
                                </IonButton>
                                <IonButton
            						className="button secondary"
                                    style={{position: 'absolute', bottom: 0, right: 0, margin: 0, width: '50%', borderLeft: '1px solid white'}}
                                    type="button"
                                    onClick={() => this.setState({
                                        showModal: false
                                    })}
                                >
                                    Close
                                </IonButton>
                    </IonContent>
                </IonModal>
                <IonButton size="small" onClick={() => this.showModal(true)}>
                    Add
                </IonButton>
            </>
        );
    }
}

export default EventAdd;
