import firebaseProvider from '../config/Firebase';

const auth = firebaseProvider.auth();
const db = firebaseProvider.firestore();

const api = {
    onSignUpUser: (
        signUpFirstName,
        signUpLastName,
        signUpEmail,
        signUpPassword
    ) => {
        return auth
            .createUserWithEmailAndPassword(signUpEmail, signUpPassword)
            .then(
                rsp => {
                    const { user } = rsp;
                    api.createNewAccount({
                        userId: user.uid,
                        signUpFirstName,
                        signUpLastName,
                        signUpEmail
                    }).then(() => {
                        auth.currentUser.updateProfile({
                            displayName: `${signUpFirstName} ${signUpLastName}`
                        });
                        return {
                            message:
                                'Your account it was successfully registered'
                        };
                    });
                },
                err => {
                    throw err;
                }
            );
    },
    createNewAccount: ({ userId, ...rest }) => {
        const body = {
            firstName: rest.signUpFirstName,
            lastName: rest.signUpLastName,
            email: rest.signUpEmail
        };
        return db.doc(`users/${userId}`).set(body);
    },
    onSignInUser: (email, password) => {
        return auth.signInWithEmailAndPassword(email, password).then(
            rsp => {
                console.log(rsp);
                return rsp;
            },
            err => {
                throw err;
            }
        );
    },
    logout: () => {
        const auth = firebaseProvider.auth();
        return auth.signOut();
    },
    fetchRestaurant: id => {
        return db
            .collection('/restaurants')
            .where('api_id', '==', id)
            .get();
    },
    fetchEventsForRestaurant: id => {
        return db
            .collection('/events')
            .where('restaurant', '==', id)
            .get();
    },
    fetchParticipantsCountForEvent: id => {
        return db
            .collection('/participants')
            .where('event', '==', id)
            .get();
    },
    joinEvent: (eventId, userId) => {
        return db.collection('/participants').add({
            event: eventId,
            user: userId
        });
    },
    leaveEvent: (eventId, userId) => {
        return db
            .collection('/participants')
            .where('event', '==', eventId)
            .where('user', '==', userId)
            .get()
            .then(querySnapshot => {
                querySnapshot.forEach(doc => {
                    doc.ref
                        .delete()
                        .then(() => {
                            console.log('Document successfully deleted!');
                        })
                        .catch(function(error) {
                            console.error('Error removing document: ', error);
                        });
                });
            })
            .catch(function(error) {
                console.log('Error getting documents: ', error);
            });
    },
    createEventForRestaurant: (event) => {
        return db.collection('/events').add(event);
    }
};

export default api;
